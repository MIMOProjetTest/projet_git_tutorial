import pandas as pd

def export_departement(num_dep):
    villes = pd.read_csv("tools/villes_france.csv", sep=",")

    vd = villes[ villes.dep == num_dep ]
    vd.to_csv(f'{num_dep}.csv', sep=";", index=False)


def population_by_dep():
    villes = pd.read_csv("tools/villes_france.csv", sep=",")
    deps = villes.groupby("dep")
    pop_dep = deps['pop2012'].sum()
    pop_dep.to_csv("population_by_dep.csv")