Le 12 mai 2021 à 17:15 nous livrons le projet chez le client
* Nous allons sauvegarder une version du projet au moment de la livraison
  pour pouvoir y faire des corrections plus tard si des bugs apparaissent
  Dans git on appelle ces versions des branches
* Dans la branche master nous continuerons les ajouts de nouvelles fonctionnalités
  qui ne seront pas livrées tant que le client ne les commande pas

## Pour lister les versions disponibles sur la machine
```
git banch

* master
```

## Création de la branche/version de production

`git checkout -b production`

```
git branch
  master
* production
```

Le 12 mai à 17h30 nous développons une nouvelle fonctionnalité
Elle doit pas aller en production
Le client ne l'a pas payée et elle n'est pas testée

* Soit on commite directement dans le master les changements, mais cela posera plein de soucis 
quand on travaille à plusieurs et c'est difficile de bien suivre quels commits correspondent 
à cette fonctionnalité si plusieurs développeurs développent des fonctionnalités différentes 
en même temps et committent aussi dans la branche master
    * Si on veut reporter uniquement cette fonctionalité en production, ce sera difficile
    d'itentifier ce qui correspond exactement à cette fonction
    * A éviter
    
* Soit on crée une branche par fonctionalité nouvellement développée
  en suivant un workflow git comme indiqué ici, par exemple:
  https://makina-corpus.com/blog/metier/2014/un-workflow-git-efficace-pour-les-projets-a-moyen-long-terme
    * On pourra alors fusionner dans le master les branches de toutes les nouvelles
    fonctionnalités
    * Et fusionner en production uniquement les fonctionalités achetées par le client
     ou les corrections de bugs
   
 ## Création de la branche pour la fonctionalité population  
```
git checkout -b population

git branch
  master
* population
  production
```
Ajouter les modification dans la nouvelle branche comme à l'habitude

# Pour envoyer les nouvelles branches sur le serveur
```
git push origin population
git push origin production

```

# Vous devez corriger un bug en production
Il faut charger sur votre PC la copie du projet qui correspond aux fichiers
livrés au client.

`git pull` récupère les fichiers de la branche courante (master par défaut)
mais cela ne récupère les nouvelles branches

Pour cela il faut exécuter
```
git fetch 

```

Ensuite vous basculez sur la branche production
`git checkout production`

* Vous faîtes votre correction  
 (dans l'idéal dans une autre branche que vous fusionnez avec production et master)
* Vous commitez 
* Vous faîtes le push




